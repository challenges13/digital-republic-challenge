
# Digital Republic Challenge

## Introduction

This repository contains a little application make to the Digital Republic Challenge, wich consists in a can of paints calculator, with some business rules. 

## First Scripts

In the project directory, you can run:

To run the application, please, clone to your machine, or doawnload the project files, and next doawload the depedencies about node modules with:

### `npm install`

Later, if everything works fine, run npm start to start the application. 

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Business Rules

1. The wall can be less than 1 square meter or more than 50 square meters, but it can have different heights and widths
1. The total area of ​​the doors and windows must be a maximum of 50% of the wall area
1. The height of walls with a door must be at least 30 centimeters higher than the height of the door
1. Each window has the measurements: 2.00 x 1.20 meters
1. Each door has the measurements: 0.80 x 1.90
1. Each liter of paint is capable of painting 5 square meters
1. I don't consider ceiling or floor.
1. The size variations of the paint cans are:

- 0.5L
- 2.5L
- 3.6 liters
- 18 liters

## Style

The style it's doing with styled components, wich is a great react library. We have a global style, wich is setup in a file at style folder, and called at the app.tsx. 

## Routes

The routes are setup wit react route, another great react library. We have an routes folder with the routes rules.

## Wall Paint Calculator Page

The main page is wall paint calculator page. There it's concentrated our application. It has four wall components, wich have a width, a height, some doors and some windows, which we use to calculate how much cans of paint the user will need. This is doing at wall paing calculator page component, by the methods handleCalculatePaint and calculateCainsOfPaint.
<br>
The handleCalculatePaint it's called when the form is submitted. So, the method clean all the results states, and ensure that the areas and heights are valid. If everything it's ok, the method call the calculateCainsOfPaint function to return how much cains the user will need. 
<br>
The calculateCainsOfPaint add the four wall areas, and next divide for 5, because 1 paint litre paints 5 square meters. Later, knowing how much paint litres the user need, the method do a great loop to define exactly cains the user need. The loop lasts until paint litres are bigger than zero. 
<br>
Back to handleCalculatePaint, now that we know how much cains the user need, it's time to put in a variable with great phrases that will be displayed for the user. That's all. 
## More Important Scripts

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it w ill copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Getting started with Git Process

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/challenges13/digital-republic-challenge.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/challenges13/digital-republic-challenge/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.