export interface  IWall  {
  height: number,
  width: number,
  windows: number, 
  doors: number,
  area: number,
  windowDoorArea: number,
  validArea: boolean, 
  validHeight: boolean,
}
