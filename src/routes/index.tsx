import { Route, Routes } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { Layout } from '../components/Layout';
import { HomePage } from '../pages/HomePage';
import { WallPaintCalculatorPage } from '../pages/WallPaintCalculatorPage';

export const Router = () =>  (
   <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<HomePage />} />
        <Route path="/calculator" element={<WallPaintCalculatorPage />} />
      </Route>
    </Routes>

   </BrowserRouter>
);
