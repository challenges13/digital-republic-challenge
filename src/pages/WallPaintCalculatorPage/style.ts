import styled from "styled-components";

export const FormStyle = styled.form`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  margin-top: 3rem;

  button {
    width: 15rem;
    margin-top: 1rem;
    margin-left: 3rem;
  }
`