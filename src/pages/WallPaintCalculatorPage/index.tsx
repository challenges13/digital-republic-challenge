import { FormEvent, useEffect, useState } from "react";
import { Button } from "../../components/Button"
import { Result } from "../../components/Result"
import { Wall } from "../../components/Wall"
import { IWall } from "../../types/IWall";
import { FormStyle } from "./style";

export const WallPaintCalculatorPage = () => {
  const [cansOfPaint, setCansOfPaint] = useState([""]);
  const [result, setResult] = useState("");
  const [wall1, setWall1] = useState <IWall>({
    height: 0,
    width: 0,
    windows: 0, 
    doors: 0,
    area: 0,
    windowDoorArea: 0,
    validArea: true, 
    validHeight: true,
  });
  const [wall2, setWall2] = useState <IWall>({
    height: 0,
    width: 0,
    windows: 0, 
    doors: 0,
    area: 0,
    windowDoorArea: 0,
    validArea: true, 
    validHeight: true,
  });
  const [wall3, setWall3] = useState <IWall>({
    height: 0,
    width: 0,
    windows: 0, 
    doors: 0,
    area: 0,
    windowDoorArea: 0,
    validArea: true, 
    validHeight: true,
  });
  const [wall4, setWall4] = useState <IWall>({
    height: 0,
    width: 0,
    windows: 0, 
    doors: 0,
    area: 0,
    windowDoorArea: 0,
    validArea: true, 
    validHeight: true,
  });

  const validAreas = wall1.validArea && wall2.validArea && wall3.validArea && wall4.validArea;
  const validHeights = wall1.validHeight && wall2.validHeight && wall3.validHeight && wall4.validHeight;

  const cleanResult = () => {
    setResult('')
    setCansOfPaint([]);
  }

  const calculateCansOfPaint = () => {
    const wallsAreas = wall1.area + wall2.area + wall3.area + wall4.area 
    let paintLitres = wallsAreas / 5 
    const cansNumber = {
      eighteen: 0,
      threePointSix: 0,
      twoPointFive: 0,
      half: 0,     
    } 

    while ( paintLitres > 0 ) {
      switch (true) {
        case paintLitres > 18:
        cansNumber.eighteen += 1 
        paintLitres -= 18
        break;
        case paintLitres > 3.6:
        cansNumber.threePointSix += 1
        paintLitres -= 3.6
        break;
        case paintLitres > 2.5:
        cansNumber.twoPointFive += 1
        paintLitres -= 2.5
        break;
        case paintLitres > 0:
        cansNumber.half += 1
        paintLitres -= .5
        break;
      }
    }
    return cansNumber;
  }

  const handleCalculatePaint = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    cleanResult()
    if (validAreas && validHeights) {
      const cansNumber = calculateCansOfPaint()
      const cansResult = [];

      cansNumber.eighteen && cansResult.push(`${cansNumber.eighteen} latas de 18L`);
      cansNumber.threePointSix && cansResult.push(`${cansNumber.threePointSix} latas de 3.6L`)
      cansNumber.twoPointFive && cansResult.push(`${cansNumber.twoPointFive} latas de 2.5L`)
      cansNumber.half && cansResult.push(`${cansNumber.half} latas de 0.5L`);

      setResult("Você precisa das seguintes latas de tinta para pintar sua sala:")
      setCansOfPaint(cansResult)
    }
  }

  useEffect(() => cleanResult(), [validAreas, validHeights])

  return (
    <>
      <header>
        <h1> Calculadora </h1>
      </header >
      <FormStyle onSubmit={(e) => handleCalculatePaint(e)}>
        <Wall
          wall={wall1}
          setWall={setWall1}        
          title="Parede 1" />
        <Wall     
          wall={wall2}
          setWall={setWall2}
          title="Parede 2" />
        <Wall        
          wall={wall3}
          setWall={setWall3}   
          title="Parede 3" />
        <Wall 
          wall={wall4}   
          setWall={setWall4}    
          title="Parede 4" />
        <Button type="submit" disabled={!validAreas || !validHeights}>
          Calcular
        </Button>
      </FormStyle>
      <Result
        visible={cansOfPaint? true: false }> 
        {result}
        <ul>
          {
            result && cansOfPaint.map((item, index) => item && <li key={index}>{item}</li>)
          }
        </ul>
      </Result>
    </>
  )
}