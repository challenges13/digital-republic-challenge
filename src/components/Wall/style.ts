import styled from "styled-components";

export const WallStyle = styled.fieldset`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 1rem;
  margin-bottom: 2rem;
  
  border: none;
  border-radius: 0.5rem;
  background-color: rgb(255, 234, 234);
  width: 49.5%;
  
  padding: 1rem;
  
  h2 {
    margin-left: 1.5rem;
  }
`