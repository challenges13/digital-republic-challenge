import { ChangeEvent, Dispatch, SetStateAction, useEffect, useState } from "react";
import { IWall } from "../../types/IWall";
import { Erro } from "../Erro";
import { Input } from "../Input";
import { WallStyle } from "./style";

type WallProps = {
  title: string;
  wall: IWall,
  setWall: Dispatch<SetStateAction<IWall>>,
}

export const Wall = ({
  title, 
  wall,
  setWall,
}:WallProps) => {
  const [errors, setErrors] = useState<string[]>([]);
  const windowArea = 2.4;
  const doorArea = 1.52;

  const validArea = (height: number, width: number, doors: number, windows: number) => {
    const area = height * width / 10000
    const windowDoorArea = doors * doorArea + windows * windowArea
    const validArea = windowDoorArea < (area / 2) && area > 1 && area < 50

    const errors:string[] = [];

    if (windowDoorArea > (area / 2))
      errors.push("A área das janelas e portas não pode ser maior que 50% da área da parede")
    if (area < 1 || area > 50) 
      errors.push("A área da parede deve possuir no mínimo 1 metro quadrado e no máximo 50 metros quadrados")

    setErrors(errors)
    return validArea
  }

  const validHeight = () => {
    const validHeight = wall.height > 220 || 
                        (wall.doors && wall.height > 220) || 
                        !(wall.doors);

    if (!validHeight) {
      setErrors([ ...errors,
        "A altura da parede que possua porta não deve ser inferior a 220cm"])
    } 
    setWall({ ...wall, validHeight })
  }
  
  const setHeight = (e: ChangeEvent<HTMLInputElement>) => {
    const newHeight = Number(e.target.value);
    const validAreaWall = 
      validArea(newHeight, wall.width, wall.doors, wall.windows)
    setWall({ 
      ...wall,
      validArea: validAreaWall,
      height: newHeight,
      area: (newHeight * wall.width) / 10000
    });
  }

  const setWidth = (e: ChangeEvent<HTMLInputElement>) => {
    const newWidth = Number(e.target.value);
    const validAreaWall = 
      validArea(newWidth, wall.height, wall.doors, wall.windows)
    setWall({ 
      ...wall,
      validArea: validAreaWall,
      width: newWidth,
      area: (newWidth * wall.height ) / 10000
    });
  }

  const setDoors = (e: ChangeEvent<HTMLInputElement>) => {
    const newDoors = Number(e.target.value);
    const decreasedDoors = newDoors < wall.doors
    const changedArea = Math.abs(wall.doors - newDoors) * doorArea

    const validAreaWall = 
      validArea(wall.height, wall.width, newDoors, wall.windows)
    
    const windowDoorArea = 
      decreasedDoors ? 
        wall.windowDoorArea - changedArea :
        wall.windowDoorArea + changedArea;

    setWall({ 
      ...wall,
      validArea: validAreaWall,
      doors: newDoors,
      windowDoorArea: windowDoorArea
    })
  }

  const setWindows = (e: ChangeEvent<HTMLInputElement>) => {
    const newWindows = Number(e.target.value);
    const decreasedWindows = newWindows < wall.windows
    const changedArea = Math.abs(wall.windows - newWindows) * windowArea

    const validAreaWall = 
      validArea(wall.height, wall.width, wall.doors, newWindows);
      
    const windowDoorArea = 
      decreasedWindows ? 
        wall.windowDoorArea - changedArea : 
        wall.windowDoorArea + changedArea;
    
    setWall({ 
      ...wall,
      validArea: validAreaWall,
      windows: newWindows,
      windowDoorArea: windowDoorArea
    })
  }

  useEffect(() => validHeight(), [wall.doors, wall.height, wall.width, wall.windows])

  return (
    <WallStyle>
      <h2>{title}</h2>
      <div>
        <Input  
          min={0}
          required       
          onChange={(e) => setHeight(e)}
          type="number" 
          placeholder="altura" />
        <Input 
          min={0}
          required
          onChange={(e) => setWidth(e)}
          type="number" 
          placeholder="largura" />
      </div>
      <div>
        <Input 
          min={0}
          onChange={(e) => setDoors(e)}
          type="number" 
          placeholder="portas" />
        <Input
          min={0} 
          onChange={(e) => setWindows(e)}
          type="number" 
          placeholder="janelas" />
      </div>
      <Erro visible={errors ? true : false} errors={errors} />
    </WallStyle>
  )
}