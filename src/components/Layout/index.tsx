import { Outlet } from "react-router";
import { Header } from "../Header";
import { Container } from "./style";

export const Layout = () => (
  <>
    <Header />
    <Container>
      <Outlet/>
    </Container>
  </>
)