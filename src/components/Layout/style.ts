import styled from "styled-components";

export const Container = styled.main`
  display: flex;
  flex-direction: column;
  width: 100%;

  padding: 4% 10% 15%;

  h1 { 
    margin-left: 3rem;
  }
`