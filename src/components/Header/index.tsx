import { NavLink  } from "react-router-dom";
import { HeaderWrapper } from "./styles";

export const Header = () => (
  <HeaderWrapper>
    <nav>
      <NavLink 
        to="/"
        className={({isActive}) => ( isActive ? "active-class": "inactive-class")}>
        Home
      </NavLink>
      <NavLink 
        to="/calculator"
        className={({isActive}) => ( isActive ? "active-class": "inactive-class")}>
        Calculator
      </NavLink>
    </nav>
  </HeaderWrapper>
)
