import styled from "styled-components";

export const HeaderWrapper = styled.header`
  display: flex;
  height: 2.8rem;
  width: 100%;  

  background-color: var(--primary-blue);
  box-shadow: 0px 2px 25px rgba(169, 169, 169, 0.8);
  
  nav {
    display: flex;
    align-items: center;
    margin: 0 1rem;

    a {
      display: flex;
      align-items: center;
      justify-content: center;
      text-decoration: none;
      font-size: 1.05rem;
      font-weight: bold;
      padding-left:  1.2rem;
      border-radius: 5px;
      transition: all 0.1s ease-in-out;
      cursor: pointer;
       &:hover {
        opacity: 0.8;
      } 
    }

    .inactive-class {
      color: var(--text-body);
      color: white;
    }

    .active-class {
      color: var(--pink);
    }
  }
`