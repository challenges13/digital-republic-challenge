import styled from "styled-components";

export const ButtonStyle = styled.button`
  background-color: #ff3f00; 
  color: white;
  border: none;
  border-radius: 0.25rem;

  cursor: pointer;
  text-align: center;
  text-decoration: none;
  font-weight: bold;
  font-size: 1rem;

  width: 8rem;
  height: 3rem;
  padding: 0.5rem 1rem;
  transition: all 0.2s ease-in-out;

  &:hover {
    opacity: 0.85;
  }
`;