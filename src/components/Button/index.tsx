import { ButtonStyle } from "./style";

type ButtonProps = {
  children: React.ReactNode;
  onClick?: () => void;
  disabled?: boolean;
  className?: string;
  type?: "submit" | "button";
};

export const Button = ({
  children,
  onClick,
  disabled = false,
  className = "",
  type,
}:ButtonProps) => (

  <ButtonStyle
    type={type}
    onClick={onClick}
    disabled={disabled}
    className={className}>
    {children}
  </ButtonStyle>

)