import { ErroWrapper } from "./style";

type ErroProps = {
  children?: React.ReactNode;
  errors: string[];
  visible?: boolean ;
}
export const Erro = ({children, visible, errors}: ErroProps) => (
  <ErroWrapper
    visible={visible}
  >
    {
      errors.map((error, index) => (
        <span key={index}>{error}</span>
      ))
    }

  </ErroWrapper>
)