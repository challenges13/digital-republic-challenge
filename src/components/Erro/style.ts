import styled from "styled-components";

type Props = {
  visible?: boolean;
};

export const ErroWrapper = styled.div<Props>`
  display: flex;
  flex-direction: column;

  width: 95%;
  font-weight: bold;
  font-size: 0.8rem;
  
  color: var(--red);
  visibility: ${props => props.visible ? "visible" : "hidden"};

`