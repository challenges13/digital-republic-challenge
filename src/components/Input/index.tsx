import { ChangeEvent } from "react";
import { InputStyle } from "./style";

type InputProps = {
  type?: string;
  name?: string;
  value?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  required?: boolean;
  className?: string;
  min?: number;

}

export const Input = ({ required, onChange, value, type, placeholder, className = "", min}: InputProps) => (
  <InputStyle
    min={min}
    required={required}
    onChange={onChange}
    value={value}
    type={type}
    placeholder={placeholder}
    className={className}
  />
)