import styled from "styled-components";

export const InputStyle = styled.input`
  width: 170px;
  height: 3rem;
  padding: 0 1.5rem ;

  border-radius: 0.25rem;
  border: 1px solid #d7d7d7;
  background-color: #e7e9ee;

  font-weight: 400;
  font-size: 1rem;

  &::placeholder {
    color: var(--text-body);
  }

  & + input {
    margin-left: 1rem;
  }
`