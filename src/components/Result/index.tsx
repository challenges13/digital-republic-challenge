import { ResultWrapper } from "./style";

type ResultProps = {
  children: React.ReactNode;
  visible: boolean;
}

export const Result = ({children, visible}: ResultProps) => (
  <ResultWrapper visible={visible}>
    <span> {children}</span>
  </ResultWrapper>

)