import styled from "styled-components";

type Props = {
  visible: boolean;
}

export const ResultWrapper = styled.div<Props>`
  padding: 0 1rem;
  font-weight: bold;
  color: #00a063;

  font-size: 1.2rem;

  visibility: ${props => props.visible ? "visible" : "hidden"};

  margin-top: 2rem;

  ul {
    margin-top: 1rem;
    color: var(--text-title);
  }
`